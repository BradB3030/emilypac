
# if you add a dependency to to requirements.txt, you need these 2 lines
# before you use those dependencies, see lib/README.md
from google.appengine.api import users
from google.appengine.ext import vendor
vendor.add('lib')
import cloudstorage

import bottle
import json
import logging
from bottle import route, post, template, error, request, response
from models import Article, SiteConfig
from core import respond
from google.appengine.ext.webapp.util import run_wsgi_app


@route('/')
def index():
    config = SiteConfig.get_entry()
    article = None
    if config.config().get("0"):
        article = Article.get_by_id(int(config.config().get("0")))
    elif config.get_buttons():
        article = Article.get_by_id(int(config.get_buttons()[0][0]))
    return respond('index.html', params={'config': config, 'article': article})

@route('/<article_id:int>')
def article(article_id):
    config = SiteConfig.get_entry()
    article = Article.get_by_id(article_id)
    if article is None:  # fallback
        article = Article.get_by_id(config.get_article_id(str(article_id)))
    return respond('index.html', params={'config': config, 'article': article})

@route('/<article_url>')
def article_by_url(article_url):
    config = SiteConfig.get_entry()
    article = Article.get_by_id(config.get_article_id(article_url))
    return respond('index.html', params={'config': config, 'article': article})


@route('/admin')
@post('/admin')
def admin():
    user = users.get_current_user()
    if not user:
        return bottle.redirect(users.create_login_url(bottle.request.path))
    logging.info("user {} is using admin".format(user.email()))
    config = SiteConfig.get_entry()
    if not users.is_current_user_admin and user.email() not in config.emails:
        return error("no admin privileges")
    articles = Article.query().fetch()
    if request.POST:
        post_config = {}
        for i in range(0, 7):
            post_config[str(i)] = request.POST.get('article_' + str(i))
            post_config['b' + str(i)] = request.POST.get('button_text_' + str(i))
            post_config['u' + str(i)] = request.POST.get('url_' + str(i))
        config.update_config(post_config)
        config.emails = request.POST.get('emails')
        config.put()
    return respond('admin.html', params={'config': config, 'articles': articles})

@post('/admin/articles')
@route('/admin/articles')
def admin_articles():
    user = users.get_current_user()
    if not user:
        return bottle.redirect(users.create_login_url(bottle.request.path))
    config = SiteConfig.get_entry()
    if not users.is_current_user_admin and user.email() not in config.emails:
        return error("no admin privileges")
    articles = Article.query().fetch()
    msg = None
    if request.POST:
        if 'article_id' in request.POST:
            article_id = int(request.POST.get('article_id'))
            for check_article in articles:
                if check_article.key.id() == article_id:
                    article = check_article
            if not article:
                return error("no article by that key")
        else:
            article = Article()
        title = request.POST.get('title')
        article_text = request.POST.get('article_text')
        article.title = title
        article.article_text = article_text
        article.put()
        msg = 'Article was saved'
    return respond('admin_articles.html', params={'articles': articles, 'msg': msg})

@route('/admin/articles/<article_id:int>')
def admin_article_single(article_id):
    user = users.get_current_user()
    if not user:
        return bottle.redirect(users.create_login_url(bottle.request.path))
    config = SiteConfig.get_entry()
    if not users.is_current_user_admin and user.email() not in config.emails:
        return error("no admin privileges")
    articles = Article.query().fetch()
    single_article = None
    for article in articles:
        if article.key.id() == int(article_id):
            single_article = article
    return respond('admin_articles.html', params={'articles': articles, 'article': single_article})

@post('/admin/imageupload')
def admin_imageupload():
    user = users.get_current_user()
    if not user:
        return error404(404)
    upload_file = bottle.request.files.get('file')
    cloudsave(upload_file.file, upload_file.filename)
    response.content_type = 'application/json'
    file_info = {'location': '/uploads/' + upload_file.filename}
    return json.dumps(file_info)

@route('/uploads/<filename>')
def uploaded_images(filename):
    return cloud_download(filename)

def cloudsave(filehandle, filename):
    filepath = '/emily-uploads/{}'.format(filename)
    write_retry_params = cloudstorage.RetryParams(backoff_factor=1.1)
    gcs_file = cloudstorage.open(filepath, 'w',
                        retry_params=write_retry_params)
    gcs_file.write(filehandle.read())
    gcs_file.close()

def cloud_download(filename):
    full_filename = '/emily-uploads/{}'.format(filename)
    with cloudstorage.open(full_filename, 'r') as comp_file:
        return comp_file.read()

# @route('/getuserdata/<user_id>')
# def getuserdata(user_id):
#     user_data = get_silly_data(user_id)
#     response.content_type = 'application/json'
#     return user_data.raw_data_as_json_str()


# @post('/postuserdata/<user_id>')
# def postuserdata(user_id):
#     posted_data = request.forms.get('posted_data')
#     try:
#         parsed_data = json.loads(posted_data)
#     except ValueError:
#         response.status = 500
#         return 'Error parsing JSON post data'

#     user_data = get_silly_data(user_id)
#     user_data.raw_data = parsed_data
#     user_data.put()
#     return 'OK'


bottle.debug(True)
# session_opts = {
#     'session.type': 'ext:google'
# }
# app = beaker.middleware.SessionMiddleware(app, session_opts)
# from google.appengine.ext.appstats import recording
# app = recording.appstats_wsgi_middleware(app)
app = bottle.app()


@error(403)
def error403(code):
    return respond('403.html')

@error(404)
def error404(code):
    return respond('404.html')
