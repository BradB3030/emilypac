import json
import logging
from google.appengine.ext import ndb


class Article(ndb.Model):
    title = ndb.StringProperty()
    image_url = ndb.StringProperty()
    article_text = ndb.TextProperty()
    status = ndb.StringProperty()

    @classmethod
    def get_by_key(keyname):
        return ndb.Key('Article', keyname).get()
    
    @property
    def id(self):
        return str(self.key.id())


class SiteConfig(ndb.Model):
    config_json = ndb.TextProperty()
    emails = ndb.TextProperty()

    @staticmethod
    def _get_key():
        return ndb.Key('SiteConfig', 'emilypac')

    @classmethod
    def get_entry(cls):
        key = cls._get_key()
        model = key.get()
        if not model:
            model = SiteConfig(key=key)
            config = {'top': None, 'articles': []}
            model.config_json = json.dumps(config)
            model.put()
        return model

    def config(self):
        return json.loads(self.config_json)

    def update_config(self, config):
        self.config_json = json.dumps(config)

    def get_buttons(self):
        config = self.config()
        buttons = []
        for i in range(1,7):
            article_id = config.get(str(i))
            url = config.get("u" + str(i))
            button_text = config.get("b" + str(i))
            if article_id and button_text:
                buttons.append((article_id, button_text, url))
        return buttons

    def get_article_id(self, article_urL):
        for button in self.get_buttons():
            if button[2] == article_urL:
                return int(button[0])
        return 1
