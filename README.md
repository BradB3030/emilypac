EmilyPAC
===========

This project started from a simple GAE starter (github.com/bradbenjamin-wf/gae_starer) to show how easy it is to get a server
running on App Engine, so you can focus on client-side aspects of your project.  Check it out!

It uses these basic technologies:

1. [GAE](https://developers.google.com/appengine/)
2. [bottle.py](http://bottlepy.org/) for handling requests, these are the @route annotations for assigning URL's to handler methods
3. [jinja2](http://jinja.pocoo.org/docs/dev/) for HTML templating in static/template/ folder

Setup for Windows
============
1. Install Python 2.7: https://www.python.org/downloads/ 
2. Install App Engine SDK for Python https://storage.googleapis.com/appengine-sdks/featured/GoogleAppEngine-1.9.40.msi (or newer)
3. Open GoogleAppEngineLauncher and add emilypac as a project ('emilypac' app name, /path/to/workspace/ as folder)

Setup for Mac
============
1. Install Python 2.7 https://www.python.org/downloads/release/python-2713rc1/
2. Install App Engine SDK for Python https://storage.googleapis.com/appengine-sdks/featured/GoogleAppEngineLauncher-1.9.49.dmg (or newer)
3. Open GoogleAppEngineLauncher and add emilypac as a project ('emilpac' app name, /path/to/workspace/ as folder)

Requirements
============
If you need to add dev dependencies, add them to requirements-dev.txt
install dev dependencies using `pip install -r requirements-dev.txt`

If you need to add runtime dependencies, add them to requirements.txt
install runtime dependencies using `pip install -r requirements.txt -t lib/`

Running Locally
============
Running locally is as simple as pressing Run in GoogleAppEngine launcher after selecting your project, then hitting
 Browse to visit the localhost URL.  When developing
 a client app, such as Android/iOS, you may find it useful to run against your localhost service. 

Deploying
============
1. You'll need to create a project at https://console.developers.google.com (ex: project_id: abcde-12345)
2. Change app.yaml application to abcde-12345
3. Run `python appcfg.py update app.yaml` (you'll have to set path to python and appcfg.py if they aren't sym-linked already)
4. Visit https://console.cloud.google.com/appengine?project=abcde-12345 to make sure the version you uploaded is the default version.

Using jinja2 templates
=======
Docs for jinja2 can be found at http://jinja.pocoo.org/docs/dev/
This project sets up a base.html which will be the structure of every page.  Then in index.html it includes base.html and it gets
that structure for free.  You don't want to duplicate your basic HTML structure, menu, and bootstrap styles in many html files, so the
include takes care of that.
It defines blocks like `{% block title %}Default{% endblock %}` with the idea that in index.html you would re-define that block and over-write
the base content.  The base template defines 4 blocks, title, extrastyle, body, and extrascripts.  Extra means optional, so for each page you
can choose only to set title and body.    

Jinja2 is great because you can avoid writing HTML in code.
 You write code to collect variables, and when you need to use them in HTML,
 you enter a code-like tools like for-loops and if-statements in template blocks.

Contribution Guidelines
=======
1. Use Bitbucket to open pull requests for review.
2. ensure code passes flake8 first

TODO List
=======
- Add images upload to cloud storage, route to serve them, admin page that lists them